//
//  Vehicle.m
//  Lab2
//
//  Created by Motiejus on 24/01/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Vehicle.h"

@implementation Vehicle

@synthesize date, location, registration, color, make, type, imgPath;

-(void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:date forKey:@"date"];
    [encoder encodeObject:location forKey:@"location"];
    [encoder encodeObject:registration forKey:@"registration"];
    [encoder encodeObject:color forKey:@"color"];
    [encoder encodeObject:make forKey:@"make"];
    [encoder encodeObject:type forKey:@"type"];
    [encoder encodeObject:imgPath forKey:@"imgPath"];
}

-(id)initWithCoder:(NSCoder *)decoder {
    if ((self=[super init])) {
        date = [decoder decodeObjectForKey:@"date"];        
        location = [decoder decodeObjectForKey:@"location"];
        registration = [decoder decodeObjectForKey:@"registration"];
        color = [decoder decodeObjectForKey:@"color"];
        make = [decoder decodeObjectForKey:@"make"];
        type = [decoder decodeObjectForKey:@"type"];
        imgPath = [decoder decodeObjectForKey:@"imgPath"];
    }
    return self;
}

- (CLLocationCoordinate2D)coordinate
{
    return location.coordinate;
}

- (NSString*)title {
    return registration;
}


@end