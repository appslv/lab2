//
//  VehicleView.h
//  Lab2
//
//  Created by Motiejus on 24/01/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#include "vehicle.h"

@interface VehicleView : UIViewController <UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CLLocationManagerDelegate>
{
    UIButton *btnRecord, *btnSelectPicture;
    UISegmentedControl *color, *type;
    UITextField *txField1, *txField2, *txField3;
    UIPickerView *pkr1;
    NSString *imgPath;
    NSMutableArray *vehicleModels;

    CLLocationManager *locationManager;
    CLLocation *location;
    NSMutableArray *vehicleList;
    
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView;
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component;
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component;

- (Vehicle*) constructVehicle;


- (IBAction)pictureButtonClicked:(id)sender;
- (IBAction)recordClicked:(id)sender;

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info;

@property (nonatomic, retain) NSString *imgPath;

@property (nonatomic, retain) IBOutlet UIButton *btnRecord, *btnSelectPicture;
@property (nonatomic, retain) IBOutlet UISegmentedControl *color, *type;
@property (nonatomic, retain) IBOutlet UITextField *txField1, *txField2, *txField3;
@property (nonatomic, retain) IBOutlet UIPickerView *pkr1;
@property (nonatomic, retain) CLLocation *location;
@property (nonatomic, retain) CLLocationManager *locationManager;
@property (nonatomic, retain) NSMutableArray *vehicleModels;

@property (nonatomic, retain) NSMutableArray *vehicleList;


@end

/*
 001		White	#FFFFFF	255,255,255
 002		Yellow	#FFFF00	255,255,0
 003		Fuchsia	#FF00FF	255,0,255
 004		Red	#FF0000	255,0,0
 005		Silver	#C0C0C0	192,192,192
 006		Gray	#808080	128,128,128
 007		Olive	#808000	128,128,0
 008		Purple	#800080	128,0,128
 009		Maroon	#800000	128,0,0
 010		Aqua	#00FFFF	0,255,255
 011		Lime	#00FF00	0,255,0
 012		Teal	#008080	0,128,128
 013		Green	#008000	0,128,0
 014		Blue	#0000FF	0,0,255
 015		Navy	#000080	0,0,128
 016		Black	#000000	0,0,0

*/