//
//  VehicleView.m
//  Lab2
//
//  Created by Motiejus on 24/01/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "VehicleView.h"
#import "Vehicle.h"
#import "VehicleArray.h"
#include "Helpers.h"
#import <MapKit/MapKit.h>


@implementation VehicleView

@synthesize btnRecord, btnSelectPicture;
@synthesize color, type;
@synthesize txField1, txField2, txField3;
@synthesize pkr1;
@synthesize imgPath;
@synthesize location;
@synthesize locationManager;

@synthesize vehicleModels;
@synthesize vehicleList;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self; // must conform to CLLocationManagerDelegate
    locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers;
    [locationManager startUpdatingLocation];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.vehicleModels = [NSMutableArray arrayWithCapacity:32];
    [self.vehicleModels addObject:@"---"];
    [self.vehicleModels addObject:@"Ford"];
    [self.vehicleModels addObject:@"Renault"];
    [self.vehicleModels addObject:@"BMW"];
    [self.vehicleModels addObject:@"Jay Z"];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [self.vehicleModels count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [self.vehicleModels objectAtIndex:row];
}

- (IBAction)pictureButtonClicked:(id)sender
{
    //create a picker
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    // allow user to pan and crop image
    imagePicker.allowsEditing = YES;
    //send messages to this view controller
    imagePicker.delegate = self;
    // select the camera as the input source
    //imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    // present the picker
    [self presentModalViewController:imagePicker animated:YES];
}

- (IBAction)recordClicked:(id)sender {
    Vehicle *vehicle = [self constructVehicle];

    [[VehicleArray instance].vehicles addObject:vehicle];
    [[VehicleArray instance] write];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    location = newLocation;
}


-(Vehicle *) constructVehicle {
    Vehicle *v = [Vehicle new];
    
    
    v.location = self.location;
    
    v.date = [NSDate date];
    v.registration = [NSString stringWithFormat:@"%@-%@-%@",
                      txField1.text,
                      txField2.text,
                      txField3.text];
    
    switch(self.color.selectedSegmentIndex) {
            case 0: v.color = [UIColor redColor]; break;
            case 1: v.color = [UIColor greenColor]; break;
            case 2: v.color = [UIColor blueColor]; break;
            case 3: v.color = [UIColor blackColor]; break;
            case 4: v.color = [UIColor whiteColor]; break;
    }
    
    NSInteger row = [self.pkr1 selectedRowInComponent:0];
    NSLog(@"Row selected: %d, filename: %@", row, self.imgPath);
    v.make = [self.vehicleModels objectAtIndex:row];
    v.type = [self.type titleForSegmentAtIndex:self.type.selectedSegmentIndex];
    v.imgPath = self.imgPath;
    return v;
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissModalViewControllerAnimated:YES];
    
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    if (!image) image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    NSString *randomName = [NSString stringWithFormat:@"image-%X%X.png",arc4random(),arc4random()];
    
    NSString *homeDirectory = [Helpers getHomeDirectory];
    
    self.imgPath = [NSString stringWithFormat:@"%@/%@", homeDirectory, randomName];
    [UIImagePNGRepresentation(image) writeToFile:self.imgPath atomically:YES];
    self.btnRecord.enabled = true;
    self.btnRecord.alpha = 1;
}
@end