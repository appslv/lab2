//
//  Vehicle.h
//  Lab2
//
//  Created by Motiejus on 24/01/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface Vehicle : NSObject <NSCoding, MKAnnotation>
{
    CLLocation *location;
    NSDate *date;
    NSString *registration;
    UIColor *color;
    NSString *make;
    NSString *type;
    NSString *imgPath;
}


-(void)encodeWithCoder:(NSCoder *)encoder;
-(id)initWithCoder:(NSCoder *)decoder;

@property (nonatomic, retain) NSString *registration;
@property (nonatomic, retain) UIColor *color;
@property (nonatomic, retain) NSString *make;
@property (nonatomic, retain) NSString *type;
@property (nonatomic, retain) NSDate *date;
@property (nonatomic, retain) CLLocation *location;
@property (nonatomic, retain) NSString *imgPath;

@end
