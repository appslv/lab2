//
//  VehicleArray.m
//  Lab2
//
//  Created by Motiejus on 12/02/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "VehicleArray.h"
#include "Helpers.h"

@implementation VehicleArray
@synthesize vehicles;

static VehicleArray *gInstance = NULL;
static NSString *const vehicleArchiveName = @"vehicles.dat";

+ (VehicleArray *)instance
{
    @synchronized(self)
    {
        if (gInstance == NULL)
            gInstance = [[self alloc] init];
    }
    
    return(gInstance);
}

-(VehicleArray*) init {
    self = [super init];
    NSString *home = [Helpers getHomeDirectory];
    NSString *archivePath = [NSString stringWithFormat:@"%@/%@", home, vehicleArchiveName];

    vehicles = [NSKeyedUnarchiver unarchiveObjectWithFile:archivePath];
    if (!vehicles) {
        vehicles = [NSMutableArray new];
    }
    return self;
}

/* Writes vehicles to disk */
-(void) write {
    NSString *home = [Helpers getHomeDirectory];
    NSString *archivePath = [NSString stringWithFormat:@"%@/%@", home, vehicleArchiveName];

    [NSKeyedArchiver archiveRootObject:vehicles toFile:archivePath];
    
}

@end
