//
//  MapView.m
//  Lab2
//
//  Created by Motiejus on 12/02/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MapView.h"
#import "VehicleArray.h"
#import "Vehicle.h"
#import "DetailedVehicleView.h"
@implementation MapView

@synthesize mapView;
@synthesize navctl;

- (id)initWithMapNavCtl:(UINavigationController*)mapNavigationController {
    self = [super init];
    navctl = mapNavigationController;
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

- (MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation
{
    MKPinAnnotationView *pinAnnotation = nil;
    static NSString *defaultPinID = @"myPin";
    pinAnnotation = (MKPinAnnotationView *)[mV dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
    if (pinAnnotation == nil)
        pinAnnotation = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:defaultPinID];
    
    pinAnnotation.canShowCallout = YES;
    
    //instatiate a detail-disclosure button and set it to appear on right side of annotation
    UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    pinAnnotation.rightCalloutAccessoryView = infoButton;
    
    return pinAnnotation;
}

-(void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {    
    Vehicle *v = (Vehicle*)view.annotation;
    
    DetailedVehicleView *detailedVehicleView = [[DetailedVehicleView alloc] initWithVehicle:v];
    
    [self.navctl pushViewController:detailedVehicleView animated:YES];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    mapView = [[MKMapView alloc] initWithFrame:self.view.bounds];
    
    [self.mapView addAnnotations:[VehicleArray instance].vehicles];
    self.mapView.delegate = self;
    
    //CLLocationCoordinate2D coord = {latitude: 61.2180556, longitude: -149.9002778};
    //MKCoordinateSpan span = {latitudeDelta: 0.2, longitudeDelta: 0.2};
    //MKCoordinateRegion region = {coord, span};
    
    //[mapView setRegion:region];
    
    [self.view addSubview:mapView];

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
