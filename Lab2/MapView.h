//
//  MapView.h
//  Lab2
//
//  Created by Motiejus on 12/02/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>


@interface MapView : UIViewController <MKMapViewDelegate> {
    MKMapView *mapView;
    UINavigationController *navctl;
}

- (id)initWithMapNavCtl:(UINavigationController*)navctl;

@property (nonatomic, retain) IBOutlet MKMapView *mapView;
@property (nonatomic, retain) UINavigationController *navctl;

@end
