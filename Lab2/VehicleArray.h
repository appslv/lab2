//
//  VehicleArray.h
//  Lab2
//
//  Created by Motiejus on 12/02/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VehicleArray : NSObject
{
    NSMutableArray *vehicles;
}

+ (VehicleArray *) instance;
- (VehicleArray *) init;
- (void) write;

@property (nonatomic, retain) NSMutableArray *vehicles;


@end
