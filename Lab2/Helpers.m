//
//  Helpers.m
//  Lab2
//
//  Created by Motiejus on 12/02/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Helpers.h"

@implementation Helpers

+ (NSString*) getHomeDirectory {
    NSArray *documents = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *homeDirectory = [documents objectAtIndex:0];
    return homeDirectory;
}


@end
