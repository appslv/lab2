//
//  AppDelegate.h
//  Lab2
//
//  Created by Motiejus on 24/01/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VehicleView.h"
#import "ListVehiclesView.h"
#import "mapView.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    VehicleView * vehicleView;
    ListVehiclesView *listVehiclesView;
    MapView *mapView;
    UITabBarController *tabBarController;
}
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) VehicleView * vehicleView;
@property (strong, nonatomic) MapView *mapView;
@property (strong, nonatomic) ListVehiclesView * listVehiclesView;
@property (strong, nonatomic) UITabBarController *tabBarController;

@end
