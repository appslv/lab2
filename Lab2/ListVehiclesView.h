//
//  ListVehiclesView.h
//  Lab2
//
//  Created by Motiejus on 12/02/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListVehiclesView : UITableViewController
{
    UINavigationController *navctl;
}

- (id)initWithNavigationController:(UINavigationController*)controller;

@property (strong, nonatomic) UINavigationController *navctl;

@end
