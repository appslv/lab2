//
//  DetailedVehicleView.h
//  Lab2
//
//  Created by Motiejus on 12/02/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Vehicle.h"

@interface DetailedVehicleView : UITableViewController
{
    Vehicle *vehicle;
}
- (DetailedVehicleView*) initWithVehicle:(Vehicle*)vehicle;

@property (nonatomic, retain) Vehicle *vehicle;

@end
